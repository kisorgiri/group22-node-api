module.exports = {
    user: {

    },
    auth: {
        forgotPassword: function (emailDetails) {
            return {
                from: '"Brodway Group 22 👻" <noreply@somethi.com>', // sender address
                to: "dprelisa@gmail.com, janakraj930@gmail.com," + emailDetails.email, // list of receivers
                subject: "Forgot Password ✔", // Subject line
                text: "Hello world?", // plain text body
                html: `
                <p>Hi <strong>${emailDetails.name}</strong>,</p>
                <p>We Noticed that you are having trouble logging into our system. Please use link below to reset your password</p>
                <p><a href="${emailDetails.link}">click here to reset your password</></p>
                <p>If you have not requested to reset your password kindly ignore this email.once the link is clicked it will not work again plase use it wisely</p>
                <p>Regards,</p>
                <p>Group22 Support Team</p>
                `
            }
        }
    }
}