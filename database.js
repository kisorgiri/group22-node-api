// mongodb is nosql database which follows
// collection based design
// highly scalable
// document based design

// to access mongo shell
// ensure mongodb is installed and running
// shell command
// mongo ==> establish connection with mongodb server
// once > arrow head interface is present we can use shell command

// show dbs ==> list all the available database
// use <db_name> ===> if db_name exists select existing datbase else create new datbaase and select it
// show collections ===> list existing collections of database
// insert into collection
// db.<col_name>.inser({validjson})
// fetch
// db.<col_name>.find({query_builder});
// db.<col_name>.find({query_builder}).pretty() // prettify content
// db.<col_name>.find({query_builder}).count() // shows count of documents


// UPDATE
// db.<col_name>.update({},{},{});
// 1st object is query builder eg color:red
// 2nd object is payload with $set as key eg {$set:{payload}}
// 3rd obect is optional options multi , upsert

//DELETE
// db.<col_name>.remove({query builder}) // dot keep empty object
// 

// drop collection
// db.<col_name>.drop();

// drop database
// db.dropDatabase();

//################### BACKUP & RESTORE#################//
// two ways of backup and restore
// bson and human redable
// backup
// command 
// mongodump ==> back up all the database of system into default dump folder
// mongodump --db <db_name> selected db will be backedup
// mongodump --db <db_name> --out  <path_to_output_directory>

// restore command
// mongorestore // it looks for dump folder and tries to restore all the database
// mongorestore <path_to_folder_containing_dump_file>

// json or csv
// json
// backupup
// command
// mongoexport
// mongoexport --db <db_name> --collection <col_name> --out <path_to_destination_folder_with .csv extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_destination_folder_with .csv extension>
// restore command 
// mongoimport 
// mongoimport --db<db_name> --collection <col_name> <path_to_source>

// csv format
// backup
// backup command ==> mongoexport
// mongoexport --db <db_name> --collection <col_name> --type=csv --fields 'comma seprated propertyname' --out <path_to_destinatin_with .csv exntension>

// restore from csv
// mongoimport  command
//  mongoimport --db <new_db_or_existing_db> --collection <col_name> --type=csv <path_toSource> --headerline


//mongodump mongorestore 
// mongoexport and mongoimport
//################### BACKUP & RESTORE#################//
