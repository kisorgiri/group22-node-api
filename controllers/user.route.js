
var router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../helpers/map_user_req');
const authorize = require('./../middlewares/authorize')
router.route('/')
    .get(function (req, res, next) {
        require('fs').readFile('sdlfkj',function(err,done){
            if(err){
                req.myEvent.emit('err',{
                    err,
                    res
                });
            }
        })
        var condition = {};
        // fetch all users
        UserModel
            .find(condition)
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            })
    })
    .post(function (req, res, next) {

    });



router.route('/dashboard')
    .get(function (req, res, next) {
        res.send('from user dashboard')

    })
    .post(function (req, res, next) {
        res.end('hi')
    })
    .delete(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })

router.route('/profile')
    .get(function (req, res, next) {
        res.send('from user profile')

    })
    .post(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })

router.route('/:id')
    .get(function (req, res, next) {
        console.log('req.loggedInuser', req.loggedInUser);

        // find single user
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            res.status(200).json(user);
        })

    })
    .delete(authorize, function (req, res, next) {

        UserModel.findOne({
            _id: req.params.id
        })
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: "User not found"
                    })
                }
                user.remove(function (err, removed) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(removed);
                })
            })
    })
    .put(function (req, res, next) {
        console.log('req.loggedInuser', req.loggedInUser);

        const data = req.body;
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not Found",
                    status: 404
                })
            }
            // user mongoose object

            var mappedUpdatedUser = MapUserReq(user, req.body);
            mappedUpdatedUser.updatedBy = req.loggedInUser.name;
            mappedUpdatedUser.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(done);
            })

        })
    })



module.exports = router;