var express = require('express');
var router = express.Router();
const UserModel = require('../models/user.model');
const MapUserReq = require('../helpers/map_user_req');
const passwordHash = require('password-hash');
const JWT = require('jsonwebtoken');
const config = require('../configs');
const randomString = require('randomstring');
const sender = require('./../configs/nodemailer.config');

const emailTemplate = require('./../templates/emailTemplate');

function createToken(data) {
    var token = JWT.sign(data, config.JWT_secret);
    return token;
}

router.post('/login', function (req, res, next) {
    UserModel.findOne({
        $or: [
            { username: req.body.username },
            { email: req.body.username }
        ]
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            var isMatch = passwordHash.verify(req.body.password, user.password);
            if (isMatch) {
                var token = createToken({
                    name: user.username,
                    role: user.role,
                    _id: user._id
                })
                res.json({
                    user,
                    token
                });
            } else {
                next({
                    msg: "Invalid Password"
                })
            }
        }
        else {
            next({
                msg: "Invalid Username"
            })
        }
    })

})

router.post('/register', function (req, res, next) {
    const newUser = new UserModel({});
    var newMappedUser = MapUserReq(newUser, req.body);
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser
        .save()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });


})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'No any account registered with provided email'
                });
            }
            var passwordResetTokenExpiry = Date.now() + 1000 * 60 * 60 * 24;
            var passwordResetToken = randomString.generate(25);
            // send email
            var data = {
                name: user.username,
                email: req.body.email,
                link: `${req.headers.origin}/reset-password/${passwordResetToken}`
            }
            var emailContent = emailTemplate.auth.forgotPassword(data);
            user.passwordResetToken = passwordResetToken;
            user.passwordResetTokenExpiry = passwordResetTokenExpiry;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(emailContent, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            });
        })
})

router.post('/reset-password/:token', function (req, res, next) {
    console.log('req.body', req.body);
    UserModel
        .findOne({
            passwordResetToken: req.params.token,
            passwordResetTokenExpiry: {
                $gte: Date.now()
            }
        })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Password reset token Invalid or Expired"
                })
            }
            // if (user.passwordResetTokenExpiry < Date.now()) {
            //     return next({
            //         msg: "password reset token expired"
            //     })
            // }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetToken = null;
            user.passwordResetTokenExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})

module.exports = router;