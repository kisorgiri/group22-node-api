
var router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../helpers/map_user_req');


router.route('/')
    .get(function (req, res, next) {
        res.send('from user dashboard')

    })
    .post(function (req, res, next) {
        //
        var newComment = new UserModel({});
        newComment.commentedBy = req.loggedInUser.name;
    })
    .delete(function (req, res, next) {

    })
    .put(function (req, res, next) {

    });

module.exports = router;