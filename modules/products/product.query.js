const ProductModel = require('./product.model');

function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.vendor)
        product.vendor = productDetails.vendor;
    if (productDetails.size)
        product.size = productDetails.size;
    if (productDetails.weight)
        product.weight = productDetails.weight;
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate;
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.offers)
        product.offers = productDetails.offers;
    if (productDetails.image)
        product.image = productDetails.image;
    if (productDetails.discountedItem) {
        product.discount = {};
        product.discount.discountedItem = productDetails.discountedItem;
        product.discount.discountType = productDetails.discountedItem ? productDetails.discountType : null;
        product.discount.discount = productDetails.discountedItem ? productDetails.discount : null;
    }
    if (productDetails.ratingPoint || productDetails.ratingMessage) {
        product.ratings = product.ratings || [];
        let ratingContent = {
            message: productDetails.ratingMessage,
            point: productDetails.ratingPoint,
            user: productDetails.user
        };
        product.ratings.push(ratingContent);
    }
}

function insert(data) {
    var newProduct = new ProductModel({});
    map_product_req(newProduct, data);
    return newProduct.save();
}

function find(condition, options = {}) {
    // projection
    var perPage = Number(options.pageSize) || 100;
    var currentPage = (Number(options.pageNumber) || 1) - 1;
    var skipCount = perPage * currentPage;
    return ProductModel
        .find(condition, {
            // discount: 0,
            // ratings: 0
        })
        .sort({
            _id: -1
        })
        .limit(perPage)
        .skip(skipCount)
        .populate('vendor')
        .exec();
}

function aggregate(condition) {
    return new Promise(function (resolve, reject) {
        ProductModel.aggregate([
            { $match: condition },
            // {
            //     $group: {
            //         _id: {
            //             day: {
            //                 $dayOfMonth: '$createdAt'
            //             },
            //         },
            //         products: { $push: '$$ROOT' },
            //         count: {
            //             $sum: 1
            //         }
            //     }
            // },
            {
                $project: {
                    category: 1,
                    interests: {
                        $multiply: ["$price", "$quantity"]
                    }
                }
            }
            // {
            //     $lookup: {
            //         from: 'users',
            //         localField: 'vendor',
            //         foreignField: '_id',
            //         as: 'users' 
            //     }
            // }
        ]
            , function (err, result) {
                if (err) {
                    reject(err);
                }
                console.log('result >>', result);
                resolve(result);
            });
    });
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id)
            .then(function (product) {
                if (!product) {
                    return reject({
                        msg: "product not found"
                    })
                }
                map_product_req(product, data);
                product.save(function (err, done) {
                    if (err) {
                        reject(err);
                    }
                    resolve(done);
                })
            })
            .catch(function (err) {
                reject(err);
            })
    })

}

function remove(id) {
    return ProductModel.findByIdAndRemove(id);
}

module.exports = {
    insert,
    find,
    update,
    remove,
    map_product_req,
    aggregate
}