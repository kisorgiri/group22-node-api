const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ratings = new Schema({
    message: String,
    point: Number,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
});

const productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    brand: String,
    category: {
        type: String,
        required: true
    },
    description: String,
    color: String,
    size: String,
    weight: Number,
    modelNo: Number,
    price: Number,
    ratings: [ratings],
    status: {
        type: String,
        enum: ['available', 'out of stock', 'booked', 'sold'],
        default: 'available'
    },
    manuDate: Date,
    expiryDate: Date,
    quantity: Number,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
        },
        discount: String
    },
    offers: {
        type: String, // sale, festive, valentine
    },
    image: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('product', productSchema);