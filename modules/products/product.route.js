const router = require('express').Router();
const ProductCtrl = require('./product.controller');
const Authenticate = require('./../../middlewares/authenticate');
const Uploader = require('./../../middlewares/uploader');

router.route('/')
    .get(Authenticate, ProductCtrl.find)
    .post(Authenticate, Uploader.single('img'), ProductCtrl.insert)

router.route('/search')
    .post(ProductCtrl.search)
    .get(ProductCtrl.search);

router.route('/:id')
    .get(Authenticate, ProductCtrl.findById)
    .put(Authenticate, Uploader.single('img'), ProductCtrl.update)
    .delete(Authenticate, ProductCtrl.remove)

module.exports = router;
