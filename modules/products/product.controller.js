const ProductQuery = require('./product.query');
const fs = require('fs');
const path = require('path');

function find(req, res, next) {
    var condition = {};
    if (req.loggedInUser.role != 1) {
        condition.vendor = req.loggedInUser._id;
    }
    ProductQuery
        .find(condition, req.query)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function insert(req, res, next) {
    console.log('req.body >>', req.body);
    console.log('req.file >>', req.file);
    var data = req.body;
    // TODO add image
    if (req.fileErr) {
        return next({
            msg: 'Invalid file format using file filter',
            status: 400
        })
    }

    if (req.file) {
        var mimeType = req.file.mimetype.split('/')[0];
        if (mimeType != 'image') {
            fs.unlink(path.join(process.cwd(), 'files/images/' + req.file.filename), function (err, done) {
                if (err) {
                    console.log('err deleteing file');
                } else {
                    console.log("file deleted")
                }
            })
            return next({
                msg: "Invalid file format",
                status: 400
            })
        }

        data.image = req.file.filename;
    }
    data.vendor = req.loggedInUser._id;
    ProductQuery
        .insert(data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function findById(req, res, next) {
    var condition = { _id: req.params.id };
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {
    var searchCondition = {};
    ProductQuery.map_product_req(searchCondition, req.body);
    console.log('req.body >>', req.body);
    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice,
            $gte: req.body.minPrice
        }
    }
    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);
        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    console.log('search condition', searchCondition);
    ProductQuery
        .find(searchCondition, req.query)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function update(req, res, next) {
    console.log('req.file >>>', req.file);
    console.log('req.body>>>', req.body)

    if (req.fileErr) {
        return next({
            msg: 'Invalid file format using file filter',
            status: 400
        })
    }


    // TODO file upload
    var data = req.body;
    data.vendor = req.loggedInUser._id;
    // for rating only
    if (req.file) {
        data.image = req.file.filename
    }
    data.user = req.loggedInUser._id;
    ProductQuery
        .update(req.params.id, data)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}


module.exports = {
    find,
    insert,
    findById,
    search,
    update,
    remove
}