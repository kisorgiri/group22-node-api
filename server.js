var express = require('express');
const path = require('path');
const cors = require('cors');
var app = express();
// express way of definging variables
app.set('port', 8080);

var port = app.get('port');

require('./db');

const event = require('events');
const myEvent = new event.EventEmitter();

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

myEvent.on('err', function (data) {
    console.log('error in application >>', data);
    // res.status(400).json(data.err)
})

require('./socket')(app);

const morgan = require('morgan');
//this app is entire express framework
// this is handler for get method and empty url
app.use(cors());
app.use(morgan('dev'));

// inbuilt middleware
app.use(express.static('files')); // serve locally within express
app.use('/file', express.static(path.join(__dirname, 'files'))); // server for external request
// server must parse every incoming data
// parse x-www-form-urlecncoded
// parse json data
// parse xml data
// parse  form -data
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
// load routing level middleware
const apiRoute = require('./routes/api.route');

app.use('/api', apiRoute);


// application level middleware but working as 404 error handler
// app.use(function (req, res, next) {
//     next({
//         msg: "Not Found",
//         status: 404
//     })
// })
// EROOR HANDLING MIDDLEWARE
app.use(function (err, req, res, next) {
    res
        .status(err.status || 400)
        .json({
            msg: err.msg || err,
            status: err.status || 400
        })
})

app.listen(port, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + port);
        console.log('press CTRL + C to exit');
    }
});

//middleware
// middleware is a function that has access to
//  http request Object 
//  http response object and
//   next middleware function
// type of middleware
// 1 application level middleware
// routing level middleware
// third party middleware // any middleware that resides in npmjs respository is thirdparty middleware
// inbuilt middleware
// error handling middleware

// syntax

// configuration block

// use method is used to place an middleware

// the order of middleware matters