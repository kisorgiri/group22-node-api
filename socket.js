const socket = require('socket.io');
const users = [];
module.exports = function(app) {
    const io = socket(app.listen(8081));
    io.on('connection', function(client) {
        var id = client.id;
        console.log('socket client connected to server');
        client.on('new-msg', function(data) {
            console.log('data in new-msg >>', data);
            client.emit('reply-msg', data); // own client
            client.broadcast.to(data.receiverId).emit('reply-msg-to', data); // expect self to all other connected client
        });
        client.on('new-user', function(data) {
            users.push({
                id,
                name: data
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        });

        client.on('disconnect', function() {
            users.forEach(function(user, i) {
                if (user.id === id) {
                    users.splice(i, 1);
                }
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
    })

}