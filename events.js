const event = require('events');
const myEvent = new event.EventEmitter();
const myEvent1 = new event.EventEmitter();

// trigger// emitters
setTimeout(function () {
    myEvent1.emit('xyz', {name:'value'});

}, 2000);
// listeners 
myEvent.on('xyz', function (data) {
    console.log('event fired >>>', data);
})