const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    //db modeling
    name: String,
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true,
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    phone: Number,
    address: {
        temp_address: [String],
        permanent_address: String
    },
    dob: Date,
    image: String,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    role: {
        type: Number,//1 admin 2 end-user 3 visitor 
        default: 2
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    passwordResetTokenExpiry: Date,
    passwordResetToken: String
}, {
    timestamps: true
})

const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;