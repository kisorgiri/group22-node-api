const mongodb = require('mongodb');
const Client = mongodb.MongoClient;
const conxnURL = 'mongodb://127.0.0.1:27017';
const dbName = 'group22db';

module.exports = {
    Client, conxnURL, dbName
}