const router = require('express').Router();
const productRoute = require('./../modules/products/product.route');
const authneticate = require('./../middlewares/authenticate');


router.use('/auth', require('./../controllers/auth.route'));
router.use('/user', authneticate, require('./../controllers/user.route'));
router.use('/product', productRoute);

module.exports = router;