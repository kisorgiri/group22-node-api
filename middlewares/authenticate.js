const JWT = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');


module.exports = function (req, res, next) {
    let token;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['token']) {
        token = req.headers['token'];
    }
    if (req.query.token) {
        token = req.query.token
    }
    if (token) {
        // hint
        var jwtToken = token.split(' ')[1];

        JWT.verify(token, config.JWT_secret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            UserModel.findById(decoded._id)
                .exec(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        req.loggedInUser = user;
                        return next();
                    } else {
                        return next({
                            msg: "User removed from system"
                        });
                    }
                })
        })
    } else {
        next({
            msg: "Token Not Provided",
            status: 400
        })
    }
}