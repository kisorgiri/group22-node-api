module.exports = function (obj1, obj2) {
    if (obj2.name)
        obj1.name = obj2.name;
    if (obj2.username)
        obj1.username = obj2.username;
    if (obj2.password)
        obj1.password = obj2.password;
    if (obj2.email)
        obj1.email = obj2.email;
    if (obj2.phone)
        obj1.phone = obj2.phone;
    if (obj2.gender)
        obj1.gender = obj2.gender;
    if (obj2.dob)
        obj1.dob = obj2.dob;
    if (obj2.role)
        obj1.role = obj2.role;
    if (obj2.status)
        obj1.status = obj2.status;
    if (obj2.temp_address || obj2.permanent_address)
        obj1.address = {
            temp_address: obj2.temp_address.split(','),
            permanent_address: obj2.permanent_address
        }
    return obj1;
}