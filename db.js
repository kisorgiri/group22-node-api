const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');
// let dbURL = mongodb://localhost:27017/my-db
mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.connection.once('open', function () {
    console.log('db connection established');
})
mongoose.connection.on('err', function (err) {
    console.log('db connection failed >>', err);
})